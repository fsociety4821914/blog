// ==UserScript==
// @name         AutoSave TiddlyWiki
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Automatically saves TiddlyWiki changes without downloading a new HTML file each time.
// @author       Your Name
// @match        file:///home/stephane/Documents/Les%20wikis/empty.htm*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Function to save TiddlyWiki changes
    function saveTiddlyWiki() {
        // Find the save button
        var saveButton = document.querySelector('[aria-label="Save"]');
        if (saveButton) {
            // Click the save button
            saveButton.click();
        }
    }

    // Save TiddlyWiki changes every 5 minutes (300000 milliseconds)
    setInterval(saveTiddlyWiki, 300000);
})();
